---
title: Index
summary: Introduction to Survey
authors:
- Antoine Masson
---

## Principle
* A survey is defined by a survey_id in uppercase and no space. It must be unique.
* A survey needs to be with at least one language activated.
* See [General Workflow](../client/index.md#general-steps) for understanding the steps in a Survey.
* Each survey has a test token (visible when editing), this token can be used to access the client and try the survey. No results are saved.

## Add/Edit survey

* When created is not possible to change the survey_id.
* See [survey general](general.md) for the general options of a survey.

## Import/Export Clone survey

You can export/import or directly clone a survey in the main survey list.


!!! note
    All the content of the survey will be saved/copied except token and answers

### Import and Export 

See [Download/Upload](downupload.md)

### Clone

* To activate the option select only one survey
1. Click on ```Clone survey``` button
1. Choose an unique survey_id

!!! note
    * When cloning a survey the attached pictures are also copied, the urls are rewritten to match the new survey

