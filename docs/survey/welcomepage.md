---
title: Welcome Page
summary: Edit Welcome page
authors:
- Antoine Masson
---

## Edit Welcome page

 You can edit the multi-language welcome page.

The manual of the text editor can be found [here](https://ckeditor.com/docs/ckeditor5/latest/features/index.html).
