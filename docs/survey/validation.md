---
title: Validation
summary: Validation of survey schema and data
authors:
- Antoine Masson
---

When saving a survey or uploading a new survey a validation is displayed.
This validation checks 3 different things : Schema, Data and Files.


## Schema

Schema validation checks that the JSON schema is correct : it checks if name and type of variables (string, number ...) are correct.

It is completely possible to have a working survey even if there are some errors (and vice and versa). An error in these parts just means that the schema contains some invalid data.


## Data

This mostly checks the validity between the prediction matrix and the objective and/or alternatives, in particular :

* That all the objectives and alternatives keys in the prediction matrix have their correspondence in objectives and alternatives
* The values in the matrix are in the range of the objective (between worst and best)

An error in this part will mostly lead to a buggy survey.


## Files

This checks that all the URLs for the pictures have a corresponding file on the server.