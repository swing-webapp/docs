---
title: Global Options
summary: Options shared among all surveys
authors:
- Antoine Masson
---
## Principle

* What is defined here can be used as global variable for each survey

## Person of Contact (POC)
* The global person of contact is used :
    * If a user has an error and no survey can be connected to this error (for example invalid token).
    * For every survey that enables global person of contact instead of entering manually.
    
* POC are defined with :
    * First and Last name (mandatory)
    * Email Address (mandatory)
    * Phone number
    * Address (all fields are mandatory if used) 
    
Email link and phone link will be automatically added when POC is displayed.

!!! note
    Changing the POC will change the global POC for every survey, even if they are created before the modification.

