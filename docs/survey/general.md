---
title: Survey General
summary: General options in Survey
authors:
- Antoine Masson
---

## Principle
* Those parameters configure the general settings of the survey.
<figure>
<img src="../../img/survey_general.png" width="90%" />
 <figcaption>Survey general example</figcaption>
</figure>

## General Options
* A survey can be Enabled or Disabled. Disabled survey cannot be accessed by any token (except the test token)
* Comments and description are only used for information and have no action on the survey itself

## Person of Contact (POC)
* You can specify to use the [global POC](global_options.md) or to specify one manually for this survey.
* POC are defined with :
    * First and Last name (mandatory)
    * Email Address (mandatory)
    * Phone number
    * Address (all field are mandatory if used)

Email link and phone link will be automatically added when POC is displayed.

## Date Options
* You can specify a starting date and time (so the survey will be available after this date).
* You can specify an ending date and time (so the survey will not be available after this date).

!!! note
    You can check the status of the survey in ACTIVE or in the list of survey (Online or Offline)
    

## External URL
* See [General Workflow](../client/index.md#general-steps) for understanding the steps in a Survey.

### Entry URL
* Enabling the entry URL will authorize the automatic creation of token using the ```/token/%tokenid%``` URL. An example is displayed

### Exit URL
* Enabling the exit URL will :
    * Disable the post survey.
    * Redirect the user after the elicitation to the specified URL.
    
It is possible to add the user token in the URL to do so insert ```%tokenid%``` and it will be replaced by the token.

An example of the generated URL (using the test token) is displayed for test.

For example ```https://www.test.com/&user=%tokenid%``` will be replaced by ```https://www.test.com/&user=atoken```.
