---
title: Objectives
summary: Add/Edit objectives used in elicitation survey
authors:
- Antoine Masson
---

## Principle

* Objectives are used in Swing and Trade-off methods. 
* Objectives can be alone (in root) or in a category.
* You can move objective (or category) within the drag and drop system.

!!! note
    If you only have one category remove it, the objective should be in root then.

!!! note
    Objectives and categories will be displayed from top to bottom.
    Categories will be displayed first then the root elements (if any).

## Add/Edit objectives

* Specify the objective short name (unique in uppercase)
* Click on ```ADD AS OBJECTIVE``` button

An objective is defined by :

* A short_name that identifies the objective (in uppercase unique)
* Enable (T/F) if false the objective is not displayed
* Description and comments (only used for admin interface information)
* A ```Best``` and a ```Worst``` values (float number)
* A multi-language ```unit```
* A method to calculate v(x) [see Methods](methods.md), only linear is possible
* An image that you can upload

You also need to provide a multi-language description by clicking on the ```EDIT TEXT``` button.
<figure>
<img src="../../../img/rational_objroot.png" width="90%" />
 <figcaption>All objectives are in root</figcaption>
</figure>

## Add/Edit categories

* Specify the category short name (unique in uppercase)
* Click on ```ADD AS CATEGORY``` button

A category is defined by :

* A short_name that identifies the objective (in uppercase unique)
* Enable (T/F) if false the category and all the objectives inside are not displayed
* Description and comments (only used for admin interface information)
* A color

You also need to provide a multi-language description by clicking on the ```EDIT TEXT``` button.

!!! note
    when moving an objective to a category, the objective will have the color of the category in the drag&drop menu

<figure>
<img src="../../../img/rational_objcat.png" width="90%" />
 <figcaption>Combination of categories and objectives</figcaption>
</figure>



## Descriptions

You also need to provide a multi-language description for all.
