---
title: Elicitation Parameters
summary: Manage Global options in elicitation suvey
authors:
- Antoine Masson
---

<figure>
<img src="../../img/rational_example.png" width="90%" />
 <figcaption>Elicitation Survey example</figcaption>
</figure>

Note : Preference elicitation surveys are also called rational surveys in this software.

## Learning loop
* Activate the learning loop in the survey. See [elicitation Workflow](../../client/index.md#elicitation-survey).

### Learning loop is ON
* Order : Specify in which order Swing and Trade-off appear.

### Learning loop is OFF

* Enabled Method : Activate Swing or Trade-off as standalone method.
* Enable restart option: If enabled, the user will be asked if he/she wants to restart the methods at the end of it (voluntary basis).


## Trade-off method

When Trade-off is activated you can specified the method used. The method for the Trade-off can be ```direct``` or ```stepwise```.

## Value function

Disabled if ```off```.

Note : Value function elicitation is also called MidValue in the software.

### Auto

In auto mode, objectives will be selected based on their weight using Swing/Trade-off to reach the specified threshold of weight. 

For instance, suppose that you have 4 objectives with 
$w_{Obj1}=0.15$, $w_{Obj2}=0.30$, $w_{Obj3}=0.40$, and $w_{Obj4} = 0.25$ (sum of $w_{Obj} = 1$). 
And the threshold is 0.6

The algorithm will sort the objectives and select objectives so the user will have to elicitate, Obj3 then Obj2 and then Obj4 (the sum of the 3 objectives weight is 0.85)

### Manual

In manual the Value function will be enabled for the selected objectives.

## Download Survey

This option enables the possibility for the user to create a report page at the end of the survey. This final report can be downloaded (as pdf, or html) or printed.


## Alternatives Analysis Text

This text is displayed on top of the graphical representation of the prediction matrix (only when learning loop is activated).

!!! note
    This text is multi-language
