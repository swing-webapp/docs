---
title: Prediction Matrix
summary: Prediction Matrix
authors:
- Antoine Masson
---

## Edit prediction matrix

* The prediction matrix is automatically generated with alternatives and objectives variables.
* When a cell is created, its value is set to 0 by default.
* To edit a value, click on it. An edit popup will appear.

<figure>
<img src="../../../img/predicmatrix_example.png" width="90%" />
 <figcaption>Example of prediction matrix</figcaption>
</figure>


## Import/Export matrix via CSV file 

* You can export and import the matrix using CSV files.
* You can download a template : the CSV has the correct format and all the values are at 0.
