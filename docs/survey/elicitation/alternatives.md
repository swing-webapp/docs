---
title: Alternatives
summary: Add/Edit alternatives used in elicitation survey
authors:
- Antoine Masson
---


## Principle

Each alternative is displayed one after another, the first alternative is on top of the list.

You can change the alternative order by clicking on ```CHANGE ODER``` button on the alternatives list.

<figure>
<img src="../../../img/alternatives_example.png" width="90%" />
 <figcaption>List of Alternatives example</figcaption>
</figure>


## Add/Edit Alternatives

An alternative is defined by

* A short_name that identifies the alternative (in uppercase unique).
* Enable (T/F) if false the alternative is not displayed.
* Description and comments (only used for admin interface information).
* An image that you can upload.

You also need to provide a multi-language description.

## Descriptions

You also need to provide a multi-language description for all.
