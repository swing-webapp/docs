---
title: Elicitation mathematical methods
summary: Brief description of used methodology in Swing and Trade-off calculation
authors:
- Antoine Masson
---

## Swing

First, user has to rank a combination of the $N_{objectives}$ in which one objective each time is improved from $\textrm{Worst}$ to $\textrm{Best}$ (and the others objectives are in $\textrm{Worst}$).

Secondly, the first chosen combination has a rate of $100$, and user can give a rate $\xi_n$ to each combination with a slider from $0$ to $100$.

Sliders are locked they cannot be higher than the weight of an upper combination and lower than the weight of a lower combination.

The final weight of each objective $\omega_n$ is given by $\omega_n = \frac{\xi_n}{\sum_{k=0}^{N_{objectives}} \xi_k}$.

## Trade-off

### Equation construction

User can choose the preference between the combination of objectives $O$ :

* $O_n(\textrm{Best})O_{n+1}(\textrm{Worst})$ (Case A)
* $O_n(\textrm{Worst})O_{n+1}(\textrm{Best})$ (Case B)
* or if both are equivalent (Case C).

#### Case A
In this case user will have to express when
$O_n(\textrm{Best})O_{n+1}(\textrm{Worst}) = O_n(\textrm{user})O_{n+1}(\textrm{Best})$.

To do so, the user uses a slider that can move between $\textrm{Worst} \leqslant \textrm{slider} < \textrm{Best}$ of $O_n$.

The following equation is constructed :

$$
\omega_n \times v_n(x=\textrm{Best}) + \omega_{n+1} \times v_{n+1}(x=\textrm{Worst}) = \omega_n \times v_n(x=\textrm{user}) + \omega_{n+1} \times v_{n+1}(x=\textrm{Best})
$$

#### Case B
In this case user will have to express when
$O_n(\textrm{Worst})O_{n+1}(\textrm{Best}) = O_n(\textrm{Best})O_{n+1}(\textrm{user})$.

To do so, the user uses a slider that can move between $\textrm{Worst} \leqslant \textrm{slider} < \textrm{Best}$ of $O_{n+1}$.

The following equation is constructed :

$$
\omega_n \times v_n(x=\textrm{Worst}) + \omega_{n+1} \times v_{n+1}(x=\textrm{Best}) = \omega_n \times v_n(x=\textrm{Best}) + \omega_{n+1} \times v_{n+1}(x=\textrm{user})
$$

#### Case C
In this case user expresses that 
$O_n(\textrm{Best})O_{n+1}(\textrm{Worst}) = O_n(\textrm{Worst})O_{n+1}(\textrm{Best})$.

The following equation is constructed :

$$
\omega_n \times v_n(x=\textrm{Best}) + \omega_{n+1} \times v_{n+1}(x=\textrm{Worst}) = \omega_n \times v_n(x=\textrm{Worst}) + \omega_{n+1} \times v_{n+1}(x=\textrm{Best})
$$

### Matrix construction

The system asks repetitively from $n=1$ to $n=N_{objectives}-1$ so we are ending up with a $N_{objectives}-1$ equation system with $N_{objectives}$ unknowns.

The normalisation $\sum_{n=1}^{N_{objectives}} \omega_n = 1$ is used to have an $n \times n$ equation system.

Those equations are inserted into a matrix

$$
\begin{pmatrix}
A_1 & A_2 & 0 & 0 & \cdots & 0 \\
0 & B_2 & B_3 & 0 & \cdots & 0 \\
0 & 0 & C_3 & C_4 & \cdots & 0 \\
\vdots & \vdots & \vdots & \cdots & \ddots & \vdots \\
0 &  0 &  0 &  \cdots & Z_{N_{objectives}-1} & Z_{N_{objectives}} \\
1 & 1 & 1 & \cdots & 1 & 1 \\
\end{pmatrix}
\times
\begin{pmatrix}
\omega_1 \\
\omega_2 \\
\omega_3 \\
\vdots \\
\omega_{N_{objectives}-1}\\
\omega_{N_{objectives}}\\
\end{pmatrix}
=
\begin{pmatrix}
0 \\
0 \\
0\\
\vdots \\
0\\
1\\
\end{pmatrix}
$$

This matrix (i.e. the equation system) is resolved using a LU solver [mathjs/lusolve](https://mathjs.org/docs/reference/functions/lusolve.html)

### $v_n(x)$ function

The $v_n(x)$, called the value function, is a function that varies from $0$ to $1$ when $x$ varies from $\mathrm{worst}$ to $\mathrm{best}$.


#### Linear

$v_n(x)$ function in linear method is given by

$$
v_n(x) = \frac{x-O_n^\mathrm{worst}}{O_n^\mathrm{best}-O_n^\mathrm{worst}}
$$


#### Exponential 
$v_n(x)$ function in exponential method is given by

$$
v_n(x) = \frac{1-Exp(-c \times Z_n(x))}{1-Exp(-c)}
$$

with :

$$
Z_n(x) = \frac{x-O_n^\mathrm{worst}}{O_n^\mathrm{best}-O_n^\mathrm{worst}}
$$

and $c$ the parameter.

If $c=0$ the linear method is used.

## Consistency rule

The consistency rule compares the weights of Swing and Trade-off there is a match when for all $n$ from $0$ to $N_{objectives}$

$$
| \omega^\textrm{swing}_n -  \omega^\textrm{tradeoff}_n | < 0.1
$$

## Final objective weight

For every category the first objective (with the highest weight) is recorded and will be used in the root calculation

The same swing and tradeoff methodology is then used for those objectives, so a set of root weight, noted $\omega_n^\mathrm{root}$ is determined.

The final weights of all the objectives are determined by the multiplication of all the weights of the category $n$ by the corresponding $\omega_n^\mathrm{root}$.

We then obtained a series of $\omega_n^\mathrm{final}$ with all the objectives (noted $\mathbb{N}$ ) of the survey with $\sum_{n=1}^\mathbb{N}  \omega_n^\mathrm{final} = 1$

## Alternative total value

The total values of each alternative are calculated using the final weight $\omega_n^\mathrm{final}$ and the prediction matrix $M_{in}$ that gives the coupling between an alternative $i$ and an objective $n$.

The total value $\Omega_i$ of an alternative $i$ is given by 

$$
\Omega_i = \sum_{n=1}^\mathbb{N} v_n(M_{in}) \times \omega_n^\mathrm{final}
$$ 


## Value function

The value function is determined during each step $n$ for an objective $k$ by :

$$
V_n = V_{n-1} \pm |\frac{O_k^\mathrm{worst}-O_k^\mathrm{best}}{4 \times (n+1)}|
$$

and 

$$
V_0 = |\frac{O_k^\mathrm{worst}-O_k^\mathrm{best}}{2}|
$$

The sign of $\pm$ is determined by the user choice


!!! note
    * Each value of $V_n$ is rounded to the step of the objective $k$
    * If $V_n = V_{n+1}$ then the algorithm stops


