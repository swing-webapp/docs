---
title: Inputs
summary: Configure input in pre/post survey
authors:
- Antoine Masson
---

## String

User can write a line of text.
If mandatory the user need to enter at least one character

## Text

User can write multiple lines of text.
If mandatory the user need to enter at least one character



## Numerical

User can only enter a number.
 
## Numerical slider
User can only enter a number selected by a slider between ```min``` and ```max```.

```step``` is the increment of the slider.
```default value``` is the value of the slider.


## Buttons

Display a series of buttons with a multi-language label, when user clicks on the button, ```value``` is recorded in results.


## Checkboxes

Display a series of checkboxes with a multi-language label, when a user selects a label, ```value``` is recorded in results.

Multiple values can be checked.

## Drag and Drop

Display a series of values with a multi-language label and the user needs to rank them.

If random start is true the order of each value is randomized each time.

If dropbox is true the user needs to move the label into a dropbox zone.
If mandatory is false, at least one element needs to be moved, if true, all of them have to be moved

