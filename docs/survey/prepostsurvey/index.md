---
title: Pre-Post survey
summary: Manage question asked before or after the rational survey
authors:
- Antoine Masson
---

## Principle

Pre- and postsurveys are composed of questions, the system integrates several kinds of inputs.

Each question is displayed one after another, the first question is on top of the list.

You can change the question order by clicking on ```CHANGE ORDER``` button on the questions list.

<figure>
<img src="../../img/postsurvey_example.png" width="90%" />
 <figcaption>Post survey list example</figcaption>
</figure>


## Add/Edit questions

A question is defined by :

* A short_name that identifies the question (in uppercase unique).
* Enable (T/F) if false the question is not displayed.
* Description and comments (only used for admin interface information).
* A question type, this allows you to choose an input type. See [Inputs](inputs.md).
* Mantatory (T/F) if true the user needs to enter something.
* Long (T/F) if true the question is display on a single page.

You also need to provide a multi-language description. 

