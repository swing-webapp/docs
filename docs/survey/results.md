---
title: Results
summary: Export results in JSON
authors:
- Antoine Masson
---

## Principle

The answers and all the tokens can be exported/downloaded for further treatment.

!!!note
    Answers are connected to tokens, so the export is a list of tokens with answers attached to each of them.

It is possible to export in JSON (original format of the data) or in CSV (columns, rows file). The CSV file is a conversion of the JSON objects in which objects (in particular answers) are flattened, so you should get every attribute of answers as columns (if the attribute is missing column will be empty).
Column values can be a direct value, a 1D or 2D array.

## Schema

The token database schema is described [dev/models/token](../development/models/token.md), this part describes the format of the export. The JSON file is a structured file that contains each field in an array where the csv contains a column for each attribute.

!!! note
    In the CSV file the object are labeled with ```.``` for example ```answers.rational.objectives.CAT0.check``` refers to the key ```answers``` then key ```rational``` then key ```objectives``` then key ```CAT0``` then attribute ```check```


## Token schema

This comes from the database models ```token``` [dev/models/token](../development/models/token.md).

* token(string) : the token string.
* survey_id(string) : the survey id's token.
* access(T/F) : true if token was created using access URL.
* status(string) can be :
    * ```new``` : token never connected.
    * ```connected``` : token in use.
    * ```finished``` : token completed the survey completely.
* answers(object) : object containing all the answers, schema described below.
* submitedAt(string) : timestamp when token has the status ```finished```.
* createdAt(Mango timestamp string) : when token is created in database.
* updatedAt(Mango timestamp string) : when token is updated in database.

## Description of answers object

* agreedata (T/F) : user agrees on data agreement.
* selectlang(string) : language chosen.
* lastupdate(integer) : last update received in epoch time.
* lasturl(string) : last url visited (mostly for internal usage when user reenter).
* presurvey(object) : presurvey results (see below).
* rational(object) : rational survey results (see below).
* postsurvey(object) : postsurvey results (see below).
* midvalue(object) : midvalue results (see below).

### presurvey(object) and postsurvey(object)

 presurvey and postsurvey share the same schema.

Each question is an object key and the result (value, array ...) the attribute.

### rational(object)
The rational object is divided into multiple objects,one for each part of the workflow.

* alternatives(array) : alternatives ranking.
* objectives(object) : objectives ranking using Swing and Trade-off.
* step(integer) : step inside the rational survey component (see step below).

#### alternatives(array)

* Alternatives is a 2D array.
* Rows give the user ranking of each alternative (first element is the most preferred).
* Columns (if any) are used if the user has to rerank the alternatives (in the conclusion) the last columns describes the last ranking.

#### objectives(object)

objectives object contains:

* the Swing / Trade-off for each category ```CATEGORY_NAME``` (object).
* the Swing / Trade-off of root ```root``` (object).
* the final objectives weight ```weight``` (object).
    * The key object is the objective_name and the attribute the calculated total value.
* the final mcda alternatives ```mcda``` (object)
    * The key object is the alternative_name and the attribute the calculated weight.

##### ```CATEGORY_NAME```(object) and root(object)

```CATEGORY_NAME``` and ```root``` use the same schema :

* swing (array of objects) : contains the swing weight for each objective :
    * The array represents each try of the methods.
    * The key object is the objective_name and the attribute is the calculated weight.
* tradeoff (array of objects) : contains the Trade-off weight for each objective (if learning loop is ON otherwise empty).
    * The array represents each try of the method.
    * The key object is the objective_name and the attribute is the calculated weight.
* check (array of strings) : Contains the result of the consistency rules and user actions (if learning loop is ON otherwise empty).
    * last array element is the last action, value can be :
    * ```good``` : Swing and Trade-off match (in this case the lastvalue will be Swing).
    * ``noretry``: user chooses not to restart (in case of manual retry).
    * ```rswing``` : no match, restart Swing.
    * ```rtradeoff``` : no match, restart Trade-off.
    * ```cswing``` : no match, continue with Swing values.
    * ```ctradeoff``` : no match, continue with Trade-off values.
    * ```cnone``` : no match, continue with uniform values.
* lastvalues (object) : contains the final weight for the objectives.
    * The key object is the objective_name and the attribute the calculated weight.
* rootobj (string) : the name of the best objective.
* step (integer) : the step in the process where the user stopped (mostly for internal usage when user reenter).

!!! note
    It is possible that a user restarts either Swing or Trade-off and there is not correspondence in the check array. This can be the case when a user reenters the survey : he/she will have to restart the complete sequence, this avoid someone to restart in a middle of Swing/Trade-off sequence for example.

    In this case, the results will lead to more Swing and Trade-off results than check elements. The latest answer will be the latest element in Swing and Trade-off results.


### valuefunc(object)

This object contains the results of the value function elicitation.

The first key is the corresponding objective.
Second key is the value function (for the moment 0.5 is implemented) and the parameter is the value of this point.


#### step(integer)

The step in ```rational``` describes where the user stopped it is mostly used for internal usage when users reconnect. To know where the user stopped :

* ```1``` : Corresponds to alternatives description.
* ```2``` : Corresponds to alternatives presentation.
* ```3``` : Corresponds to alternatives ranking.
* ```4``` : Corresponds to objectives presentation.
* ```5 up 5+ncat``` : Corresponds to objective category ranking (```ncat``` is the number(positive or null integer) of categories in the survey).
* ```6+ncat``` : Corresponds to root objective ranking.
* ```7+ncat``` : Corresponds to conclusion.
