---
title: Admin Users
summary: Manage Admin Users
authors:
- Antoine Masson
---

## List admin users

* This module displays admin users (they can access the admin interface)
* You can edit and remove users
* **All admin users can access all options and surveys**

## Add/Edit admin user
A user is defined by :

* an email address as ```login```
* a password
* can be Enabled\Disabled, if disabled the login will be not permitted 
*  ```first name```, ```last name```, and ```comments``` are used only for display
