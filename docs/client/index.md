---
title: Index
summary: Client principle and workflow
authors:
- Antoine Masson
---

## Principle

* The client uses the information provided in the admin interface.
* The client interface does not check that all required values are correct and present, so if at least one value is incorrect the interface will be locked at some point.
* As soon as the token is validated, the database receives the user's answers, so normally nothing is lost.

## URL

* Token can be entered in the login box or provided automatically in ```/token/%tokenid%``` URL.
* If access token is enabled, the token is automatically created and user is directed to survey. The token is provided in ```/access/%tokenid%``` URL.

## Workflow / Steps

### General steps

```mermaid
graph TD;
    A[Login page] --> AA{is token valid and survey online?}
    AD[use /token/%tokenid%  URL] --> AA;
    AA ---->|Yes| B{Survey Language >1};
    AA ---->|No| A;
    AA ---->|Token is finished| A;
    AA ---->|Token is connected and survey online| AAA[Send to last step];
    B---->|Yes| BB[Select a language];
    B---->|No| C;
    BB --> C[Display welcome page];
    C --> D{User accepts the data agreement};
    D ---->|No| A;
    D ---->|Yes| E[Do Pre Survey];
    E --> F[Do Elicitation Survey];
    F --> G{exit url enabled?}
    G ----> |No| H[Do Post Survey];
    G ----> |Yes| E1[Send to the specified URL with token added];

    H --> E0[END];
    E1 --> E0[END];
    subgraph if access enabled and survey online
    AE1[use /access/%tokenid% URL] --> AE2{Token exists};
    AE2 ----> |No| AE3[Token is created];
    AE2 ----> |Yes| AE3B[Send to last step];
    AE3 --> F;
    end

```

### Elicitation Survey

#### General Algorithm

```mermaid
graph TD;
RS0[Start] --> A1[Display Alternative descriptions];
A1 --> A2[Display Alternatives];
A2 -- User clicked on all alternatives --> A3[User sorts Alternatives];
A3 --> O1{Contains Categories?};
O1 ----> |Yes| OC1[Display Category descriptions];
OC1 --> OCE1[Swing Trade-off];
OCE1 --> OCE2{Other Category?};
OCE2 ----> |Yes| OC1;
OCE2 ----> |No| OR1;

O1 ----> |No| OR1[Root Objectives];
ORR[Top Objective of each Category or Root Objective] --> OR1

OR1 --> ORE1[Swing Trade-off];


ORE1 --> F1[Display Final Ranking]
F1 --> F2{Learning Loop?};
F2 ----> |No| MV;
F2 ----> |Yes| F3[Prediction Matrix representation];
F3 --> F4[Calculated Alternatives ranking];
F4 --> F5{Are Alternatives manual and calculated rankings similar?};

F5 ----> |Yes| MV{Value function enabled?};
F5 ----> |No| F6[User sorts Alternatives];
F6 -->MV; 

MV ----> |Yes| MV1[User does Value function];
MV1 --> DW

MV ----> |No| DW{Download survey enabled?};

DW ----> |No| FE[END];
DW ----> |Yes| DW1[User can download results];
DW1 --> FE;

```

#### Swing Trade-off Algorithm
```mermaid
flowchart TD;
RS0[Start] --> DO[Display Objectives];
DO -- User clicked on all objectives --> LL{Learning loop?};

LL ----> |Yes| LL1{Do Swing and Trade-off <br>order as requested};

LL1 ----> |Swing| SW1
LL1 ----> |Trade-off| TO1


LL ----> |No| LL2{Swing or Trade-off selected?};

LL2 ----> |Swing| SW1
LL2 ----> |Trade-off| TO1


SW1[Start Swing]
SW1 --> SW2{Already displayed Swing presentation};

SW2 ----> |No| SW3[Display Swing Presentation];

SW2 ----> |Yes| SW4[Swing Sorting];

SW3 --> SW4;
SW4 --> SW5[Swing Rating];
SW5 --> SWE[End of Swing]

TO1[Start Trade-off]
TO1 ----> TO2{Already displayed Trade-off presentation};;
TO2 ----> |No| TO3[Display Trade-off Presentation];
TO2 ----> |Yes| TO4[Loop over n-1 objectives];
TO3 --> TO4;

TO4 --> TO5[Preference Ranking]
TO5 --> TO6{Is Stepwise enabled?}
TO6 ----> |No| TO8[Equivalence evaluation];
TO6 ----> |Yes| TO7[Region ranking];
TO7-->TO8
TO8 --> TO4

TO4 --loop finished --> TOE[End Trade-off]


SWE --> OC11;
TOE --> OC11;


OC11[Display Category results];
OC11 --> OC12{Learning Loop?};
OC12 ----> |Yes| OC13{Are Trade-off and Swing similar?};
OC13 ----> |No| OC14{n>1};
OC14 ----> |no| OC15{user repeats};
OC15 ----> |swing| SW1;
OC15 ----> |trade-off| TO1;
OC14 ----> |yes| OC16{user selects values};
OC16 ----> |yes| OCE1;
OC16 ----> |no| OC15;
OC13 ----> |Yes| OCE1;
OC12 ----> |No| OC17{Redo enabled?}
OC17 ----> |yes| OC15;
OC17 ----> |no| OCE1;

OCE1[END of Category];
```

#### Value function Algorithm
```mermaid
flowchart TB;
MD0[Start Value function];
MD0 --> MD1[Loop over activated Objectives];
MD1 --> MD2[Value is displayed]
MD2 --> MD3{User agrees with value?}
MD3 ---> |yes| MD1
MD3 ---> |no| MD4[User can choose to decrease or increase value]
MD4 -- new value is calculated --> MD2

MD1 -- loop over --> MDE

MDE[END of Value function];
```
