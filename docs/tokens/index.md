---
title: Index
summary: Manage Tokens for accessing client survey
authors:
- Antoine Masson
---

## Principle

* A token is a chain of characters (case-sensitive) that will be used to recognize a user accessing a survey from the client application.
* Each token must be unique and related to only one survey.
* Each survey contains a random token to test it, no answers are sent to the database.
* A token can have 3 states :
    * ```new``` : the token exists in the database but nothing happened to it.
    * ```connected``` : the token is connected and can contain answers.
    * ```finished``` : the survey associated with the token has been finished.
    
* A user with ```finished```  token cannot access the survey anymore, whereas a user with a ```connected``` token can come back and continue.
* When resetting a token to ```new```, related answers and submitted timing are erased.
* A token can be Enabled/Disabled. A user with a disabled token cannot login, answers are preserved.
* The times of creation, of the last update and of when the last answer was submitted are recorded.

## Add/Edit tokens

* When editing a token the token itself cannot be changed anymore.
* If the token contains answers, it is possible to display them by clicking on the ```Show answers``` button.
* When removing a token, related answers are also removed


