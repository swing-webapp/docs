---
title: Batch Tokens
summary: How to create multiples tokens at once
authors:
- Antoine Masson
---

Batch token allows you to create multiple tokens at once by providing a list of tokens. You can set the survey, or you can create a list for multiple surveys at once too.

!!! note
    In the case of batch token each token will have the status of ```new```. It is still possible to edit the token manually afterwards.

## Create multiple tokens

You can access the batch token module by going to ```Token > Add Tokens in batch``` in the main menu or when displaying a token list (either from the main menu or in a specific survey).

### Manually set survey id

If you want to set manually the survey, put the ```Set Survey_id manually``` to ```true``` then select the survey.

!!! note
    If you are editing a survey and access the module from there, the `survey_id` is already set and cannot be changed.

1. Insert the list in the box, in the case of Manually set survey id the list contains only the token, each token is separated by a new line (See example below).
1. Click to ```Next``` to visualize the processing of the list.
1. If the list is correct click ```Proceed``` or you can still change the list by clicking ```previous```.
1. When processing the result is displayed for each token ```OK``` if it is created or ```Error``` with an explanation otherwise.

#### List example
    
    tokena
    machin
    blabla
    hello
    test0

### Survey id in the list

Put the ```Set Survey_id manually``` to ```false```.

!!! note
If you are editing a survey and access the module from there, the `survey_id` is already set and cannot be changed.

1. Insert the list in the box. In this case, the list contains the token, and the corresponding `survey_id` separated with the character ```,```. Each element is separated by a new line (See example below).
2. Click to ```Next``` to visualize the processing of the list.
3. If the list is correct click ```Proceed``` or you can still change the list by clicking ```previous```.
4. When processed, if the token was successfully created ```OK``` is displayed. Else ```Error``` is displayed together with an explanation.

#### List example

    tokena,EXAMPLE
    machin,EXAMPLE
    blabla,SURVEYB
    hello,SURVEYC
    test0,SURVEYC
