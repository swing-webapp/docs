---
title: Project documentation
summary: Index of the documentation
authors:
- Antoine Masson
---
# Welcome to ValuePreferences Documentation
This documentation is divided in several parts.

* [Admin](./admin/index.md) :
    * Configure the admin interface, users ... 
    * [Admin User management](./admin/users.md)
* [Development](./development/index.md) :
    * For developers
    * About source code, functions, database models, systems used ...
* [Languages](./languages/index.md) :
    * [How to manage languages in survey, edit global variables](./languages/index.md)
* [Survey](./survey/index.md) :
    * [How to Add/Edit survey](./survey/index.md)
    * [Edit Global Options](./survey/global_options.md)
    * [How to edit the survey welcome page](./survey/welcomepage.md)
    * [How to manage pre and post survey](./survey/prepostsurvey/index.md)
    * [How to manage elicitation of preference survey](./survey/elicitation/index.md)
    * [How to export answers](./survey/results.md)
* [Token](./tokens/index.md) :
    * How to create access for users in the client interface
    * [Manually](./tokens/index.md) or in [Batch](./tokens/batch.md)
