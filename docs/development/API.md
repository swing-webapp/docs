---
title: API
summary: Information regarding the API
authors:
- Antoine Masson
---

## Authentication
Can be 

* ```none``` : accessible to public
* ```CLIENT``` : accessible with a client token
* ```ADMIN``` : accessible with an admin token

Authentication is done by adding a http Authorization bearer header with the token ```config.headers['Authorization'] = `Bearer ${ token }```.

## General


* Inputs are x-www-form-url encoded in body.

### root /
* Action : Return the version of the API
* URL : ```/```
* Method : ```GET```  
* Input : ```none```
* Auth :   ```none```
* Output : ```JSON```

Output:

```javascript
{
    message: "API Eawag Version 1.0.7",
    status: "OK"
}
```

## Survey Commands

### Admin side

#### List all surveys
* Action : Get surveys list
* URL : ```/surveys/all```
* Method : ```GET```
* Input : ```none```
* Auth :   ```ADMIN```
* Output : ```JSON```

Output:

```javascript
{
    surveys: [
        {
            general: {
                dates: {
                 date_start: "2021-04-07T15:00:00.000Z",
                 date_stop: "2021-04-07T15:05:00.000Z",
                 date_start_enable: false,
                 date_stop_enable: false
                },
            externalurl: {
                    pointofentry: "rational",
                    pointofexit: "rational",
                    enableeentryurl: true,
                    enableexiturl: true,
                    exiturl: "http://test.com?token=%tokenid%"
                },
                enable: true,
                languages: [
                    "ENGLISH"
                ],
                short_name: "TEST",
                description: null,
                comments: null,
                poc: {
                    useglobal: false,
                    _id: "603669e6778980752582f5b4",
                    firstname: "Antoine",
                    lastname: "AA",
                    email: "antoine@amassomn.ccc",
                    id: "603669e6778980752582f5b4"
                }
            },
            createdAt: "2021-02-24T14:59:50.769Z",
            updatedAt: "2021-04-21T12:19:19.490Z",
            status: true,
            poc: {
                useglobal: false,
                _id: "605af683f82eee001467e790",
                firstname: "Alice",
                lastname: "Aubert",
                email: "alice.aubert@eawag.ch",
                id: "605af683f82eee001467e790"
            },
            id: "603669e6778980752582f5b3"
        },
        ...
    ],
    message: "",
    status: "OK"
}
```
    
#### Create a survey
* Action : Add a new survey into the database
* URL : ```/surveys/create```
* Method : ```POST```
* Input : ```survey model``` See [SurveyModel](./models/survey.md)
* Auth :   ```ADMIN```
* Output : ```JSON```

Output:

```javascript
{
    survey: {
        general: {
            externalurl: {
                pointofentry: "rational",
                pointofexit: "rational"
            },
            enable: true,
            languages: [], 
            short_name: "TESTPOSTMAN2",
            description: "Created with postman",
            poc: {
                useglobal: true,
                _id: "60ae6b8fa8327a2990cb795c",
                 firstname: "truc",
                 lastname: "machin",
                 email: "antoine@machin.ch",
                 id: "60ae6b8fa8327a2990cb795c"
            }
        },
        rational: {
            general: {
                conclusiontext: {
                    content: []
                },
                learnloop: true
            },
            alternatives: {
                content: [], 
                items: []
            },
            objectives: {
                content: [],
                items: []
            }
        },
        welcome: [],
        presurvey: [],
        postsurvey: [],
        createdAt: "2021-05-26T15:38:55.813Z",
        updatedAt: "2021-05-26T15:38:55.813Z",
        status: true,
        poc: {},
        id: "60ae6b8fa8327a2990cb795b"
    },
    message: "Survey created", 
    status: "OK"
}
```
#### Get a survey
* Action : Get all the survey fields 
* URL : ```/surveys/survey/<surveyid>```
* Method : ```GET```
* Input : ```none```
* Auth :   ```ADMIN```
* Output : ```JSON```

Output:

```javascript
{
    survey: {
        ...
    },
    message: "",
    status: "OK"
}
```
#### Download a survey
* Action : Download the contents of a survey
* URL : ```/surveys/downsurvey/<surveyid>```
* Method : ```GET```
* Input : ```none```
* Auth :   ```ADMIN```
* Output : ```FILE```

Output: ZIP file with JSON ```survey.json``` + attached pictures in folders. See [Download/Upload](../survey/downupload.md).

#### Upload a survey
* Action : Upload the contents of a survey
* URL : ```/surveys/upload```
* Method : ```POST```
* Input : ```upload```:ZIP file and ```newid```:new short name for the file, otherwise shortname in ```survey.json```used. 
* Auth :   ```ADMIN```
* Output : ```JSON```
    * survey : content of the created survey
    * Validation : files : returns the number of missing file(s) and deleted, schema : array of schema errors, Data : array of data errors. See [Validation](../survey/validation.md).

Output:

```javascript
{
    survey:{...surveycontent...}
    id: "60108b3ecfcea004fc6de5f4",
    message: "Survey created",
    status: "OK"
    validation:{files:{missing:0,deleted:0},schema:[],data:[]}
}
```

#### Clone a survey
* Action : Copy the contents of a survey
* URL : ```/surveys/clone```
* Method : ```POST```
* Input : ```srcsn```: Short_name of survey to be cloned ,  ```newsn```: new short name
* Auth :   ```ADMIN```
* Output : ```JSON```
    * survey : content of the created survey
 
Output:

```javascript
{
    survey:{...surveycontent...}
    id: "60108b3ecfcea004fc6de5f4",
    message: "Survey created",
    status: "OK"
}
```


#### Update a survey
* Action : Modify a survey
* URL : ```/surveys/survey/<surveyid>```
* Method : ```PUT```
* Input : ```survey model```. See [SurveyModel](./models/survey.md).
* Auth :   ```ADMIN```
* Output : ```JSON```
    * Validation : files : returns the number of missing file(s) and deleted, schema : array of schema errors, Data : array of data errors. See [Validation](../survey/validation.md).

Output:

```javascript
{
    id: "60108b3ecfcea004fc6de5f4",
    message: "Survey modified",
    status: "OK"
    validation:{files:{missing:0,deleted:0},schema:[],data:[]}
}
```

#### Enable a survey
* Action : Enable a survey
* URL : ```/surveys/survey/enable/<surveyid>```
* Method : ```PUT```
* Input : ```none```
* Auth :   ```ADMIN```
* Output : ```JSON```

Output:

```javascript
{
    id: "60108b3ecfcea004fc6de5f4",
    message: "Survey enabled",
    status: "OK"
}
```

#### Disable a survey
* Action : Disable a survey
* URL : ```/surveys/survey/disable/<surveyid>```
* Method : ```PUT```
* Input : ```none```
* Auth :   ```ADMIN```
* Output : ```JSON```

Output:

```javascript
{
    id: "60108b3ecfcea004fc6de5f4",
    message: "Survey disabled",
    status: "OK"
}
```

#### Delete a survey
* Action : Delete a survey
!!! note
    Corresponding tokens are also removed.

* URL : ```/surveys/survey/<surveyid>```
* Method : ```DELETE```
* Input : ```none```
* Auth :   ```ADMIN```
* Output : ```JSON```

Output:

```javascript
{
    id: "60108b3ecfcea004fc6de5f4",
    message: "Survey successfully deleted",
    status: "OK"
}
```

#### Update global options
* Action : Set surveys global options
* URL : ```/surveys/options```
* Method : ```POST```
* Input : ```global options keys```. See [SurveyModel](./models/survey.md)
* Auth :   ```ADMIN```
* Output : ```JSON```

Output:

```javascript
{
    message: "Survey global options updated",
    status: "OK"
}
```

#### Get global options
* Action : Get surveys global options
* URL : ```/surveys/options```
* Method : ```GET```
* Input : ```none```
* Auth :   ```ADMIN```
* Output : ```JSON```

Output:

```javascript
{
    options: {
        _id: 0,
        poc: {
            address: {
                street: "Rue du jura 7",
                zipcode: 1004,
                city: "Lausanne"
            },
            useglobal: false,
            _id: "602fe5c22865564c5d469d87",
            firstname: "antoine",
            lastname: "masson",
            email: "antoine@youmi-lausanne.ch"
        },
        __v: 0
    },
    message: "",
    status: "OK"
}
```

#### Drop survey table
* Action : Force mango to reload model
* URL : ```/surveys/drop```
* Method : ```GET```
* Input : ```none```
* Auth :   ```ADMIN```
* Output : ```JSON```

Output:

```javascript
{
    message: "Survey drop",
    status: "OK"
}
```

### Attached Image Management

#### Post new image for a survey
* Action : POST new image into API storage (survey)
* URL : ```/surveys/survey/<:surveyid>/picture/<:lang>/<:objname>``` or ```/surveys/survey/<:surveyid>/picture/<:lang>/<:objname>/<subobjname>```
* Method : ```POST```
* Input : ```file```
* Auth :   ```ADMIN```
* Output : ```JSON```

Remarks : 

* The submitted filename is used to save the file in the structure. If no filename is provided, the API tries to generate a filename based on the mimetype.
* URL inputs are strings and are not validated by the API anyhow.


Output:

```javascript
{
    url: "https://....",
}
```


### Get image for a Survey
* Action : GET image (filename) from API storage (survey)
* URL : ```/surveys/survey/<:surveyid>/picture/<:lang>/<:objname>/<filename>``` or ```/surveys/survey/<:surveyid>/picture/<:lang>/<:objname>/<subobjname>/<filename>```
* Method : ```GET```
* Input : ```none```
* Auth :   ```none```
* Output : ```file```


#### POST new image for a language translation
* Action : POST new image into API storage (language translation)
* URL : ```/surveys/langtrans/picture/<:lang>/<:id>```
* Method : ```POST```
* Input : ```file```
* Auth :   ```ADMIN```
* Output : ```JSON```

Remarks : 

* The submitted filename is used to save the file in the structure. If no filename is provided, the API tries to generate a filename based on the mimetype.
* URL inputs are strings and are not validated by the API anyhow.


Output:

```javascript
{
    url: "https://....",
}
```


#### Get image for a language translation
* Action : GET image (filename) from API storage (language translation)
* URL : ```/surveys/langtrans/picture/<:lang>/<:id>/<filename>```
* Method : ```GET```
* Input : ```none```
* Auth :   ```none```
* Output : ```file```



### Language management admin side (langtrans)

#### List languages 
* Action : Return all the activated languages
* URL : ```/surveys/langtrans```
* Method : ```GET```
* Input : ```none```
* Auth :   ```ADMIN```
* Output : ```JSON```

Output:

```javascript
{
    langtrans:[
        {
            language: "FRENCH",
            flag: "fr",
            status: "TODO",
            id: "607565b3f390ea617f3d5110"
        },
        ...
    ]
    message: "",
    status: "OK"
}
```

#### Create a language
* Action : Create a new language
* URL : ```/surveys/langtrans```
* Method : ```POST```
* Input :
    - language(string) : language name
    - flag(string) : flag name
    - fields (JSON) : table of fields translated 
        - field(string) : variable name
        - text(string) : translation of this variable
* Auth :   ```ADMIN```
* Output : ```JSON```

Output:

```javascript
{
    langtrans: {
        language: "NOVALANG",
        flag: "FR",
        fields: [
            {
                _id: "60b67cbaa8327a2990cb7962",
                field: "WELCOME",
                text: "Welcome",
                id: "60b67cbaa8327a2990cb7962"
            }
        ],
            createdAt: "2021-06-01T18:30:18.681Z",
            updatedAt: "2021-06-01T18:30:18.681Z",
            status: "TODO",
            id: "60b67cbaa8327a2990cb7961"
    },
        message: Language Translation created",
        status: "OK"
}
```

#### Update a language
* Action : Update a  translation
* URL : ```/surveys/langtrans/<varid>```
* Method : ```PUT```
* Input :
    - language(string) : language name
    - flag(string) : flag name
    - fields (JSON) : table of fields translated
        - field(string) : variable name
        - text(string) : translation of this variable
* Auth :   ```ADMIN```
* Output : ```JSON```

Output:

```javascript
{
        id:"60b67cbaa8327a2990cb7961"
        message: "Language Translation modified",
        status: "OK"
}
```
#### Delete a language
* Action : Delete a  translation
* URL : ```/surveys/langtrans/<varid>```
* Method : ```DELETE```
* Input : ```none```
* Auth :   ```ADMIN```
* Output : ```JSON```

Output:

```javascript
{
        id:"60b67cbaa8327a2990cb7961"
        message: "Language Translation successfully deleted",
        status: "OK"
}
```

#### Get a language
* Action : Return the variables of asked language
* URL : ```/surveys/langtrans/<langname>```
* Method : ```GET```
* Input : ```none```
* Auth :   ```CLIENT or ADMIN```
* Output : ```JSON```

Output:

```javascript
{
    {
        langtrans: {
            language: "ENGLISH",
            flag: "gb",
            fields: [
                {
                    _id: "60801b8d73087bea6cc02ed8",
                    field: "NEXT",
                    text: "Next",
                    id: "60801b8d73087bea6cc02ed8"
                },
            ...
        ],
        updatedAt: "2021-04-21T12:33:17.908Z",
        createdAt: "2021-04-21T12:33:17.908Z",
        status: "TODO",
        id: "607565b3f390ea617f3d510f"    
    },
    message: "",
    status: "OK"
}
```

#### Save langtrans definition file

* Action : Save the langtrans database into ```API.template_dir+'LanguageTrans.zip'```
* URL : ```/surveys/savelangtrans```
* Method : ```GET```
* Input : ```none```
* Auth :   ```ADMIN```
* Output : ```ZIP```

Output:

```javascript
{
        message: "Translation file saved",
        status: "OK"
}
```


#### Load langtrans definition file

* Action : Load ```API.template_dir+'LanguageTrans.zip'``` into the langtrans database
* URL : ```/surveys/loadlangtrans```
* Method : ```GET```
* Input : ```none```
* Auth :   ```ADMIN```
* Output : ```JSON```

Output:

```javascript
{
        message: "Translation file loaded",
        status: "OK"
}
```

#### Download language translation
* Action : Make a ZIP file from langtrans database and pictures
* URL : ```/surveys/downlangtrans```
* Method : ```GET```
* Input : ```none```
* Auth :   ```ADMIN```
* Output : ```file```

Output:

```
    LanguageTrans.zip
```
#### Upload language translation
* Action : Upload a ZIP file and erase langtrans database with it
* URL : ```/surveys/uploadlangtrans```
* Method : ```POST```
* Input : ```ZIP File```
* Auth :   ```ADMIN```
* Output : ```JSON```

Output:
```javascript
{
        message: "Translation file uploaded",
        status: "OK"
}
```

### Variables template management admin side (langdef)

#### List variables
* Action : Get all the variables
* URL : ```/surveys/langdef```
* Method : ```GET```
* Input : ```none```
* Auth :   ```ADMIN```
* Output : ```JSON```

Output:

```javascript
{
    langdefs:[
        {
            type: "String",
            field: "WELCOME",
            description: "Welcome text",
            createdAt: "2021-01-20T21:56:23.148Z",
            updatedAt: "2021-01-20T21:56:23.148Z",
            id: "606f6cda4847b14e0fa06afc"
        },
        ...
    ]
    message: "",
    status: "OK"
}
```
#### Create a variable
* Action : Create a new variable
* URL : ```/surveys/langdef```
* Method : ```POST```
* Input :
    - field (string) : variable name
    - description (string) : description of the variable
    - type (string) : can be ```String``` (simple text) or   ```html``` (html text editor)
* Auth :   ```ADMIN```
* Output : ```JSON```

Output:

```javascript
{
    {
        langdef: {
            type: "String",
            field: "WELCOME3",
            description: "welcome text",
            createdAt: "2021-06-01T17:59:25.075Z",
            updatedAt: "2021-06-01T17:59:25.075Z",
            id: "60b6757da8327a2990cb7960"
    },
        message: "Language Definition created",
        status: "OK"
    }
}
```
#### Update a variable
* Action : Update a  variable
* URL : ```/surveys/langdef/<varid>```
* Method : ```PUT```
* Input :
    - field (string) : variable name
    - description (string) : description of the variable
    - type (string) : can be ```String``` (simple text) or   ```html``` (html text editor)
* Auth :   ```ADMIN```
* Output : ```JSON```

Output:

```javascript
{
        id:"60b6757da8327a2990cb7960"
        message: "Language Definition modified",
        status: "OK"
}
```

#### Delete a variable
* Action : Delete a  variable
* URL : ```/surveys/langdef/<varid>```
* Method : ```DELETE```
* Input : ```none```
* Auth :   ```ADMIN```
* Output : ```JSON```

Output:

```javascript
{
        id:"60b6757da8327a2990cb7960"
        message: "Language Definition successfully deleted",
        status: "OK"
}
```

#### Save langdef definition file
* Action : Save the langdef database into ```API.template_dir+'LanguageDef.json'```
* URL : ```/surveys/savelangdef```
* Method : ```GET```
* Input : ```none```
* Auth :   ```ADMIN```
* Output : ```JSON```

Output:

```javascript
{
        message: "Configuration file saved",
        status: "OK"
}
```


#### Load langdef definition file
* Action : Load ```API.template_dir+'LanguageDef.json'``` into the langdef database
* URL : ```/surveys/loadlangdef```
* Method : ```GET```
* Input : ```none```
* Auth :   ```ADMIN```
* Output : ```JSON```

Output:

```javascript
{
        message: "Configuration file loaded",
        status: "OK"
}
```

#### Download variables definition
* Action : Make a JSON file from langdef database
* URL : ```/surveys/downlangdef```
* Method : ```GET```
* Input : ```none```
* Auth :   ```ADMIN```
* Output : ```file```

Output:

```
    LanguageDef.json
```
#### Upload variables definition
* Action : Upload a JSON file and erase langdef database with it
* URL : ```/surveys/uploadlangdef```
* Method : ```POST```
* Input : ```JSON File```
* Auth :   ```ADMIN```
* Output : ```JSON```

Output:
```javascript
{
        message: "Configuration file uploaded",
        status: "OK"
}
```


### Client side

#### Get survey key

!!! note
    Keys are adapted in function of the language specified in the token.

* Action : Get survey specific key  (welcome, presurvey, postsurvey, rational). See [SurveyModel](./models/survey.md).
* URL : ```/surveys/me/get/<key>```
* Method : ```GET```
* Input : ```none```
* Auth :   ```CLIENT```
* Output : ```JSON```

Output:

```javascript
{
    payload: <key content>
    message: "<key>",
    status: "OK"    
}
```

#### Get survey info
* Action : Get survey general info (poc, languages, externalurl)
* URL : ```/surveys/me/infos```
* Method : ```GET```
* Input : ```none```
* Auth :   ```CLIENT```
* Output : ```JSON```

Output:

```javascript
{
    payload: {
        poc: {
            useglobal: false,
            _id: "603669e6778980752582f5b4",
            firstname: "Antoine",
            lastname: "AA",
            email: "antoine@amassomn.ccc"
        },
        languages: [
            {
                id: "ENGLISH",
                flag: "gb"
            }
        ],
            externalurl: {
                pointofentry: "rational",
                pointofexit: "rational",
                enableeentryurl: true, 
                enableexiturl: true,
                exiturl: "http://test.com?token=%tokenid%"
        }
    },
    message: "info", 
    status: "OK"
}
```

#### Set selected language
* Action : return language variables and corresponding token for a given language.

!!! note
    You need a new token in order for the API to know the selected language (which is a data inside the token)

* URL : ```/surveys/me/languages/<langID>```
* Method : ```POST```
* Input : ```none```
* Auth :   ```CLIENT```
* Output : ```JSON```

Output:

```javascript
{
    langtrans:{
        NEXT: "Next",
        WELCOME: "Welcome",    
            ...    
    },
    token: "...",
    message: "token updated",
    status: "OK"
    
}
```

## Token commands

### Admin side

#### Get token list
* Action : Get array of all token
* URL : ```/tokens/all```
* Method : ```GET```
* Input : ```none```
* Auth :   ```ADMIN```
* Output : ```JSON```

Output:

```javascript
{
        tokens: [
            {
                enable: true,
                test: false,
                access: false,
                token: "test0",
                survey_id: "603669e6778980752582f5b3",
                status: "connected",
                comments: null,
                createdAt: "2021-02-28T12:41:05.646Z",
                updatedAt: "2021-04-08T20:10:10.358Z",
                answers: {
                    selectlanguage: "ENGLISH",
                    agreedata: false,
                    lastupdate: 1617912610324,
                    lasturl: "/survey/welcome.htm"
                },
                id: "<tokenid>"
            },
            ...
                ],
    message: "",
    status: "OK"
}
```

#### Create token
* Action : Create a new token
* URL : ```/tokens/register```
* Method : ```POST```
* Input : [token model](./models/token.md)
* Auth :   ```ADMIN```
* Output : ```JSON```

Output:

```javascript
{
    token:{
            enable: true,
            test: false,
            access: false,
            token: "test0",
            survey_id: "603669e6778980752582f5b3",
            status: "connected",
            comments: null,
            createdAt: "2021-02-28T12:41:05.646Z",
            updatedAt: "2021-04-08T20:10:10.358Z",
            id: "<tokenid>"
        },
        message: "Token created",
        status: "OK"
}
```

#### Get a token
* Action : Get token info
* URL : ```/tokens/token/<tokenid>```
* Method : ```GET```
* Input : ```none```
* Auth :   ```ADMIN```
* Output : ```JSON```

Output:

```javascript
{
    token:{
            enable: true,
            test: false,
            access: false,
            token: "test0",
            survey_id: "603669e6778980752582f5b3",
            status: "connected",
            comments: null,
            createdAt: "2021-02-28T12:41:05.646Z",
            updatedAt: "2021-04-08T20:10:10.358Z",
            answers: {
                selectlanguage: "ENGLISH",
                    agreedata: false,
                    lastupdate: 1617912610324,
                    lasturl: "/survey/welcome.htm"
            },
            id: "<tokenid>"
        },
        message: "",
        status: "OK"
}
```

#### Modify token
* Action : Modify a token
* URL : ```/tokens/token/<tokenid>```
* Method : ```PUT```
* Input : [token model](./models/token.md)
* Auth :   ```ADMIN```
* Output : ```JSON```

Output:

```javascript
{
        id : "<tokenid>"
        message: "Token modified",
        status: "OK"
}
```


#### Delete token
* Action : Delete a token
* URL : ```/tokens/token/<tokenid>```
* Method : ```DELETE```
* Input : ```none```
* Auth :   ```ADMIN```
* Output : ```JSON```

Output:

```javascript
{
        id : "<tokenid>"
        message: "Token successfully deleted",
        status: "OK"
}
```

#### Get token of a survey
* Action : Get token of survey
* URL : ```/tokens/survey/<surveyid>```
* Method : ```GET```
* Input : ```none```
* Auth :   ```ADMIN```
* Output : ```JSON```

Output:

```javascript
{
        tokens: [
            {
                enable: true,
                test: false,
                access: false,
                token: "test0",
                survey_id: "603669e6778980752582f5b3",
                status: "connected",
                comments: null,
                createdAt: "2021-02-28T12:41:05.646Z",
                updatedAt: "2021-04-08T20:10:10.358Z",
                answers: {
                    selectlanguage: "ENGLISH",
                    agreedata: false,
                    lastupdate: 1617912610324,
                    lasturl: "/survey/welcome.htm"
                },
                id: "<tokenid>"
            },
            ...
                ],
    message: "",
    status: "OK"
}
```

#### Get test token of a survey
* Action : Get test token of survey
* URL : ```/tokens/survey/<surveyid>/test```
* Method : ```GET```
* Input : ```none```
* Auth :   ```ADMIN```
* Output : ```JSON```

Output:

```javascript
{
    tokens:{
            enable: true,
            test: true,
            access: false,
            token: "0b459a6fba",
            survey_id: "60361c6ac9e7196193adb9dd",
            status: "new",
            comments: "Automatically created by API",
            createdAt: "2021-02-24T09:29:14.853Z",
            updatedAt: "2021-02-24T09:29:14.853Z",
            id: "<tokenid>"
        },
        message: "",
        status: "OK"
}
```

#### Download JSON of token for given survey 
* Action : Get JSON file of token for given survey 
* URL : ```/tokens/downjson/<surveyid>```
* Method : ```GET```
* Input : ```none```
* Auth :   ```ADMIN```
* Output : ```File```

#### Download CSV conversion of token for given survey
* Action : Get CSV file of token for given survey (converted from JSON)
* URL : ```/tokens/downcsv/<surveyid>```
* Method : ```GET```
* Input : ```none```
* Auth :   ```ADMIN```
* Output : ```File```

### Client side

#### Login
* Action : Login admin user
* URL : ```/tokens/login```
* Method : ```POST```
* Input : ```token ```
* Auth :   ```none```
* Output : ```JSON```

Output:

```javascript
{
    token: "...", 
    message: "Succeeded Login", 
    status: "OK"
}
```

#### Access login
* Action : Create a new token with direct access
* URL : ```/tokens/access```
* Method : ```POST```
* Input : ```survey_id```, ```token```
* Auth :   ```none```
* Output : ```JSON```

!!! note
    Only accessible when access token is enabled on the survey.

Output:

```javascript
{
    token: "...", 
    message: "Succeeded Login",
    status: "OK"
}
```

#### Get User info
* Action : give info about used token
* URL : ```/tokens/me```
* Method : ```GET```
* Input : ```none```
* Auth :   ```CLIENT```
* Output : ```JSON```

Output:

```javascript
{
    user: {
            id: "603b8f616fbde221723a7e42", 
            token: "test0",
            survey_id: "603669e6778980752582f5b3",
            status: "connected",
            access: false,
            admin: false,
            test: false,
            answers: {
                selectlanguage: "ENGLISH",
                agreedata: false,
                lastupdate: 1617912610324,
                lasturl: "/survey/welcome.htm",
                presurvey: {},
                postsurvey: {},
                rational: {}
            },
            iat: 1619461489,
            exp: 1619547889
    },
    message: "", 
    status: "OK"
}
```

#### Update User status
* Action : update status of used token
* URL : ```/tokens/me/status```
* Method : ```PUT```
* Input : ```status```
* Auth :   ```CLIENT```
* Output : ```JSON```

Output:

```javascript
{
    message: "Token status modified", 
    status: "OK"
}
```

#### Update User answers
* Action : update answers of used token
* URL : ```/tokens/me/answers```
* Method : ```PUT```
* Input : ```answers```. [See answers/result](../survey/results.md)
* Auth :   ```CLIENT```
* Output : ```JSON```

Output:

```javascript
{
    message: "Token answers modified", 
    status: "OK"
}
```

#### Get User Answers
* Action : get answers of used token
* URL : ```/tokens/me/answers```
* Method : ```GET```
* Input : ```none```
* Auth :   ```CLIENT```
* Output : ```JSON```

Output:

```javascript
{
    answers: {
            selectlanguage: "ENGLISH",
            agreedata: false,
            lastupdate: 1617912610324,
            lasturl: "/survey/welcome.htm",
            presurvey: {},
            postsurvey: {},
            rational: {}
            },
    message: "", 
    status: "OK"
}
```

## Users commands

### Admin side

#### Get users list
* Action : Get array of all admin users
* URL : ```/adminusers/all```
* Method : ```GET```
* Input : ```none```
* Auth :   ```ADMIN```
* Output : ```JSON```

Output:

```javascript
{
        users: [{
            admin: true,
            enable: true,
            username: "...@youmi-lausanne.ch",
            lastname: "masson",
            firstname: "antoine",
            createdAt: "2021-04-26T13:52:35.630Z",
            updatedAt: "2021-04-26T13:52:35.630Z",
            id: "<userid>
    },
            ...
                ],
    message: "",
    status: "OK"
}
```

#### Login
* Action : Login admin user
* URL : ```/adminusers/login```
* Method : ```POST```
* Input : ```username, password```
* Auth :   ```none```
* Output : ```JSON```

Output:

```javascript
{
    token: "...", 
    message: "Succeeded Login", 
    status: "OK"
}
```

#### Register
* Action : Create a new admin user
* URL : ```/adminusers/register```
* Method : ```POST```
* Input : [user model](./models/users.md)
* Auth :   ```ADMIN```
* Output : ```JSON```

Output:

```javascript
{
        token: "...",
        user: {
            admin: true,
            enable: true,
            username: "...@youmi-lausanne.ch",
            lastname: "masson",
            firstname: "antoine",
            createdAt: "2021-04-26T13:52:35.630Z",
            updatedAt: "2021-04-26T13:52:35.630Z",
            id: "<userid>
    },
        message: "User created",
        status: "OK"
}
```

#### Get the connected admin user info
* Action : give info about used token
* URL : ```/adminusers/me```
* Method : ```GET```
* Input : ```none```
* Auth :   ```ADMIN```
* Output : ```JSON```

Output:

```javascript
{
    user: {
            id: "<userid>", 
            username: "...@youmi-lausanne.ch",
            firstname: "antoine",
            lastname: "masson",
            comments: null,
            admin: true,
            iat: 1619445055,
            exp: 1619531455
    },
    message: "", 
    status: "OK"
}
```

#### Modify the connected admin user
* Action : modify info about used token
* URL : ```/adminusers/me```
* Method : ```PUT```
* Input : [user model](./models/users.md)
* Auth :   ```ADMIN```
* Output : ```JSON```

Output:

```javascript
{
    message: "User modified",
    status: "OK"
}
```

#### Modify an admin user
* Action : Modify an admin user
* URL : ```/adminusers/user/<userid>```
* Method : ```PUT```
* Input : [user model](./models/users.md)
* Auth :   ```ADMIN```
* Output : ```JSON```

Output:

```javascript
{
    
        id: "<userid>",
        message: "User modified",
        status: "OK"
    
}
```

#### Get an admin user
* Action : give info about a user
* URL : ```/adminusers/user/<userid>```
* Method : ```GET```
* Input : ```none```
* Auth :   ```ADMIN```
* Output : ```JSON```

Output:

```javascript
{
    user: {
            admin: true,
            enable: false,
            username: "...@youmi-lausanne.ch",
            lastname: "masson",
            firstname: "antoine",
            createdAt: "2021-04-26T14:04:53.838Z",
            updatedAt: "2021-04-26T14:05:04.639Z",
            comments: "This is a comment about me jj",
            id: "<userid>"
    },
    message: "", 
    status: "OK"
}
```

#### Delete an admin user
* Action : delete an admin user
* URL : ```/adminusers/user/<userid>```
* Method : ```DELETE```
* Input : ```none```
* Auth :   ```ADMIN```
* Output : ```JSON```

Output:

```javascript
{
    id: "<userid>",
    message: "User successfully deleted",
    status: "OK"
}
```

#### At least one user admin
* Action : return true if at least one admin exists in the database
* URL : ```/adminusers/exist```
* Method : ```GET```
* Input : ```none```
* Auth :   ```none```
* Output : ```JSON ```

Output:

```javascript
{
    adminpresent: true,
    message: "",
    status: "OK"
}
```
#### Register first admin user
* Action : Create first admin user
* URL : ```/adminusers/createfirst```
* Method : ```POST```
* Input : [user model](./models/users.md)
* Auth :   ```none```
* Output : ```JSON```

!!! note
    Disabled when at least one admin user exists in the database : this operation is used when creating a new instance.

Output:

```javascript
{
        token: "...",
        user: {
            admin: true,
            enable: true,
            username: "...@youmi-lausanne.ch",
            lastname: "masson",
            firstname: "antoine",
            createdAt: "2021-04-26T13:52:35.630Z",
            updatedAt: "2021-04-26T13:52:35.630Z",
            id: "<userid>
    },
        message: "User created",
        status: "OK"
}
```

### Client side

None (as client cannot manage admin users)
