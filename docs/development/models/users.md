---
title: User
summary: Database model for admin users
authors:
- Antoine Masson
---

## Schema
``` javascript
    const AdminUserSchema =  new Schema({
        firstname: {
            type: String,
            required:true
        },
        lastname: {
            type: String,
            required:true
        },
        admin: {
            type:Boolean,
            default: true
        },
        username: {
            type: String,
            validate: [validator.isEmail, "Please provide a valid email address"],
            required: [true, 'Username is required'],
            unique: true
        },
        password: {
            type: String,
            required: "Please include your password"
        },
        comments: {
            type: String
        },
        enable: {
            type: Boolean,
            required: true,
            default:true
    }
    })
```
