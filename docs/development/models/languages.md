---
title: Languages
summary: Database model for languages
authors:
- Antoine Masson
---

## SurveyLanguageDef schema 

```javascript
field:{
    type:String,
    required:true,
},
description:{
    type:String,
},
type:{
    type:String,
    default: "String",
    enum:['String','html'],
    required:true,
}

```


## SurveyLanguageTrans schema
``` javascript
language:{
    type:String,
    required:true,
},
flag:{
    type:String,
    required:true,
},
fields:[{
    field: {
        type: String,
        required: true,
    },
    text:{
        type: String,
        required: true,
    },

    }]
```
