---
title: Token
summary: Database model for token
authors:
- Antoine Masson
---

## Schema
``` javascript
    const TokenSchema =  new Schema({
    token: {
        type: String,
        required:true
    },
    survey_id: {
        type: String,
        required:true
    },
    comments: {
        type: String
    },
    enable: {
        type: Boolean,
        required: true,
        default:true
    },
    test: {
        type: Boolean,
        required: false,
        default:false
    },
    access: {
        type: Boolean,
        required: true,
        default:false
    },
    status: {
        type: String,
        enum:['new','connected','finished'],
        required:true
    },
    answers:{
        type : Object,
    },
    submitedAt:{
        type: Date
    }})
```

* token : the token string
* survey_id : the survey's token (_id)
* test : true only for survey token test
* access : true if token was created using access url
* status can be :
    * ```new``` : token never connected
    * ```connected``` : token in use
    * ```finished``` : token completed the survey completely
* answers : object containing all the answers, schema in [Survey/results](../../survey/results.md)    
* submitedAt : timestamp when token has the status ```finished```
* createdAt(Mango timestamp) : when token is created in database
* updatedAt(Mango timestamp) : when token is updated in database
