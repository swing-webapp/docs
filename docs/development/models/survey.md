---
title: Survey
summary: Database model for survey
authors:
- Antoine Masson
---

## Survey schema

```javascript
    general:{
        short_name: {
            type: String,
            required: true,
            unique:true,
        },
        description: {
            type: String,
        },
        comments: {
            type: String,
        },
        enable: {
            type: Boolean,
            required: true,
            default: true
        },
        poc: POCSchema,
        dates:{
            date_start:{
                type:Date
            },
            date_stop:{
                type:Date
            },
            date_start_enable:Boolean,
            date_stop_enable:Boolean,
        },
        languages:{
            type:Array
        },
        externalurl:{
            enableeentryurl:Boolean,
            pointofentry:{
                type : String,
                default: "rational"},
            enableexiturl:Boolean,
            pointofexit:{
                type : String,
                default: "rational"},
            exiturl:String,
        }
    },
    welcome:
        [{
            language:String,
            html:{
                content:String
            },
            movie:{
                url:String
            }
        }],
      presurvey:[PrePostSurveySchema],
      postsurvey:[PrePostSurveySchema],
    rational:{
        general: {
            learnloop:{
                type :Boolean,
                required: true,
                default: true,
            },
                       prop_retry:{
                type :Boolean,
                default: false,
            },
            swing:{
                type :Boolean,
                required: true,
                default: true,
            },
            tradeoff:{
                type :Boolean,
                required: true,
                default: false,
            },
            tradeoff_method:{
                type: String,
                enum:['direct','stepwise'],
                required:true
            },
            
            midval:{
                general:{
                    type :String,
                    enum:['off','auto','manual'],
                    required: true,
                    default: 'off',
                },
                auto_threshold:{
                    type: Number,
                    default : 0.6,
                }
                
            },

            order:{
                type :String,
                enum:['ST','TS'],
                default: 'ST',
            },
            download_weight:{
                type :Boolean,
                required: true,
                default: false,
            },
            conclusiontext:{
                content:[text],
            }

        },
        alternatives:{
            content:[text],
            items:[altschema]
        },
        objectives: {
            content:[text],
            items:[objschema],
        },
        predicmat:Object,
    },
```

* general
    * short_name(string) : survey identifier
* rational      
    * predimat : Object with alternative as first key and objectives as second key, value is the coupling between.
    

## POC schema

```javascript
    firstname: {
        type: String,
    },
    lastname: {
        type: String,
    },
    useglobal: {
        type:Boolean,
        required:true,
        default: false
    },
    email: {
        type: String,
    },
    address : {
        street:String,
        zipcode : Number,
        city : String,
        country : String
    },
    phonenumber:Number,
```

## PrePostSurvey schema

```javascript
    _id:mongoose.ObjectId,
    short_name: {
        type: String,
        required: true,
    },
    description: {
        type: String,
    },
    comments: {
        type: String,
    },
    enable: {
        type: Boolean,
        required: true,
        default: true
    },
    mandatory:{
        type: Boolean,
        required: true,
        default: false
    },
    long:{
        type: Boolean,
        required: true,
        default: false
    },
    type : {
        type:String,
        required: true
    },
    type_attribut:Object,
    contents: [textinputs],
```

* See [prepostsurvey](../../survey/prepostsurvey/index.md) and [prepostsurvey/inputs](../../survey/prepostsurvey/inputs.md)

## altschema (Schema for alternatives)

```javascript

        _id:mongoose.ObjectId,
        short_name: {
            type: String,
        },
        description: {
            type: String,
        },
        comments: {
            type: String,
        },
        enable: {
            type: Boolean,
            default: true
        },
        elements:{
            picture: String,
            contents:[textcaption],
            },
```

## objschema (Schema for objectives)

```javascript
        _id:mongoose.ObjectId,
        short_name: {
            type: String,
        },
        description: {
            type: String,
        },
        comments: {
            type: String,
        },
        enable: {
            type: Boolean,
            default: true
        },
        children:Array,
        type:{
            type: String,
            enum:['objective','category'],
            required:true
        },
        elements:Object,
```

* children : Free schema for objective and category
    * common
    
        ```javascript
            {
              enable:{type: Boolean},
              children: [],
              _id: {type: mongoose.ObjectId},
              short_name: {type: String},
              description: {type: String},
              comments: {type: String},
              type: {type: String, enum:['objective','category']},
              id: {type: String}
              elements: {type:Object}  
            }
        ```

    - objectives

        ```javascript
            midval_manual_enable:{type: Boolean},
              elements: {
                picture:  {type:String},
                contents: [text],
                min: {type:Float},
                max: {type:Float},
                step: {type:Float},
                unit: {type:Object},
                interp: {type:String},
                interpC: {type:Float},
              },
        ```

        - midval_manual_enable : if true the objective is selected for manual midvalue
        - picture : URL of image
        - min,max,step : used in Trade-off
        - unit : object key is language and value the text corresponding to unit.
        - inter : interpolation method for V(x)  ```linear```  or ```exp``` are possible.
            - If using ```exp``` : ```interpC```is the parameter of the function.
    
    - category
          ```javascript
    
                  elements: {
                    color: {type:String},
                    contents: [text],
                  },
            ```
        - color : css color

## Extra schema

### textinputs

```javascript
            language:String,
            html:{
                content:String,
                inputs:String,
            }
```

### text

```javascript
            language:String,
            html:{
                content:String,
            },
```

### textcaption

```javascript
        language:String,
        html:{
            caption:String,
            content:String,
        }
```
