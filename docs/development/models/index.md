---
title: Database Models
summary: Manage Global options in rational suvey
authors:
- Antoine Masson
---

Database models describes the schema of each table in the mongo database

* For [languages](languages.md)
* For the [survey](survey.md)
* For the [token](token.md)
* For the [user admin](users.md)
