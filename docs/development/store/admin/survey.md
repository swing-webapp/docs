---
title: survey
summary: Store functions in admin/survey
authors:
- Antoine Masson
---

## General
This store manages everything regarding survey (list, creation ...) but also editing survey.
When editing a survey the survey data are stored in the state and only send to the API when user decides to (click on save button).

## State
```javascript
{
    surveys:[], // list of surveys
    loading:false, // is list loading?
    translist:[], // list of translation
    trans:[], // specific translation edit
    templatevar:[], // list of template variables
    status:null, // true if last command was without error
    globalopt:{}, // global survey options
    editsurvey:{ // edit survey storage
        id:null, // survey id
        origin_data:null, //not used
        updated_data:null, //data of edited survey (updated when modified)
    }
};

```

## Getters


### Global
* ```getGlobalPOC``` : return global Person Of Contact
* ```getGlobal``` : return global options
* ```getIdtoSn``` : return an object of id : short_name
* ```getSntoId``` : return an object of short_name : id
*  ```getSelectListSurveys``` : return a list of surveys for selector (label, id, description)
* ```getTableSurveys``` : generate list for survey table
* ```getSelectListLanguages```: return a list of languages for selector (label, value, icon)

### In Edit survey

* ``getQuestion(pre,id)`` : get specific pre/post survey question
* ```getAlternatives(id)``` : get specific alternative
* ```getAlternativeDes``` : get description of alternative
* ```getRaitionalObj``` : return list of objectives
* ```getObjectiveDes``` : get description of objective
* ```getRationalGen```  : get general options of rational
* ```getUpdateSurvey(field)``` : return specific key of updated survey
* ```getTableQuestions(pre)``` : return table of questions for pre/post survey
* ```getTableAlternatives``` : return table of alternatives
* ```getListAltandObj``` : return an object with list of objectives and alternatives shortnames
* ```getListObj``` : return a list of objectives
* ```getPredicMat``` : return the prediction matrix as object




## Actions
Only not obvious actions are documented here



## Mutations
Only not obvious mutations are documented here
