---
title: auth
summary: Store functions in admin/auth
authors:
- Antoine Masson
---

## General

This store manages login and logout functions for admin interface.

## State

```javascript

{
    user: null, // user info from jws token
    token: null, //jws token
};
```
## Getters

* ```isAuthenticated``` : return true if a token is present
* ```user``` : return user info (from jws token)
* ```id``` : return user id
* ```isTokenValid``` : return true if token is valid (not outdated)

## Actions

* ```LogIn(token)``` : if login is valid save token and decode it
*  ```LogOut(token)``` : disconnect and remove data



## Mutations
Only not obvious mutations are documented here.
