---
title: index
summary: Store functions in admin 
authors:
- Antoine Masson
---

## General
* The store uses vuex for sharing functions among the entire project.
* The store enables logging in console when not in production.

* The store is divided in three namespaced modules :
    * [auth](auth.md) : About admin user management
    * [survey](survey.md) : About survey management
    * [token](token.md) : About token management
