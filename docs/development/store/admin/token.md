---
title: token
summary: Store functions in admin/token
authors:
- Antoine Masson
---

## General

This store manages token (creation, edit ...) from global or a specific survey.

## State

```javascript
{
    editsurvey:{ // specific token if in edit survey mode
        tokens:null, // list of tokens
        test:null, // is the token test
        survey_id:null, // survey id
        short_name:null, // survey short name
    },
    loading:false, // true if list is loading
    globaltoken:[], // list of global token
    global:true, // true if token is global, false if in edit survey mode
    status:false // is true if last command was without error
}
```

## Getters

* ```getList``` : return list of token global or survey specific
* ```getStatus``` : return status

## Actions

* ```getTokenList(global,survey_id,short_name)``` : get list of tokens from API
* ```updateTokenList``` : get list of token from API using previous settings
* ```getSurveyToken(survey_id)``` : get token for specific survey
* ```getSurveyTest(survey_id)``` : get token test for specific survey
* ```getGlobalTokenList ``` : get the whole list of tokens
*  ```downSurveyJSON ``` : trigger download json file for survey
*  ```downSurveyCSV ``` : trigger download csv file for survey
*  ```addToken(form)``` : add token to API from form
* ```addTokenBatch(tab)``` : create tokens in batch from tab
* ```updateToken(id,form)``` : update a given token
* ```deleteToken(id)``` : delete a given token
* ```deleteTokens(ids)``` : delete multiple given tokens
* ```getToken(id)``` : get info from a specific token
* ```changeEnableToken(ids,state)``` : update enable status of given tokens


## Mutations
Only not obvious mutations are documented here
