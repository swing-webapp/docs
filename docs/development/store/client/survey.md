---
title: survey
summary: Store functions in client/survey
authors:
- Antoine Masson
---

## General
This store manages communication with API regarding survey data loading and user input load and send

## State
```javascript
{

    userinput:{ //user input on the different sections
        selectlanguage:null, //selected language
        agreedata:null, //user agrees on data
        lastupdate:null, //timestamp
        lasturl:null, // lastvisited url
        presurvey:{},
        postsurvey: {},
        rational:{}

    },
    data:{ //survey data
        info:null, //general info
        templatetrans:null, //variables translation
        welcome:null, //welcome page
        presurvey:null,
        rational:null,
        postsurvey:null
    },
    loading:{ //is true if data from section has been loaded
        info:null,
        templatetrans:null,
        welcome:null,
        presurvey:null,
        rational:null,
        postsurvey:null,

    }
};

```

## Getters
* ```getdata(key)``` : return the data of a specific key
* ```selectlanguage``` : return user selected language
* ```getuserinput(key)``` : return the userinput of a specific key


## Actions

* ```ReloadAll``` : Reload all the data from API (used in test mode)
* ```clearSurvey``` : clear all the state
* ```getUserInput``` : get user previous input from API
* ```setUserInput``` : send user previous input to API
* ```setLanguage(langid,reentry)``` : specify the selected language, and download corresponding translation
* ```getkey``` : download a specific survey key from API
* ```loadkey``` : download a specific survey key from API if needed



## Mutations
Only not obvious mutations are documented here
