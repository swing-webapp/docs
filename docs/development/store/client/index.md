---
title: index
summary: Store functions in client 
authors:
- Antoine Masson
---

## General
* The store uses Vuex for sharing functions among the entire project.
* The store enables logging in console when not in production.

* The store is divided in two namespaced modules :
    * [auth](auth.md) : About user login and access management
    * [survey](survey.md) : About survey access management
