---
title: auth
summary: Store functions in client/auth
authors:
- Antoine Masson
---

## General
This store manages communication with API regarding token logging and validation.
## State

```javascript

{
    user: null, // : contents of the jws token 
    id:null, // tokenid
    token: null, // jws token
    message:{ //  used if a message is displayed in login pages
        title:null,
        message:null,
        poc:null,
}
};

```


## Getters

* ```istest``` : return true if token is a test, false otherwise (normal token)
* ```isAuthenticated``` : return true if a token is present
* ```user``` : return user info (from jws token)
* ```userid``` : return user id
* ```isTokenValid``` : return true if token is valid (not outdated)

## Actions

* ```Login(token)``` : if token is valid redirect to Surveyindex
* ```Access(token)``` : if access is on for the survey, create token and redirect to Surveyindex
* ```logerror``` : generate a message according to logging error
* ```setToken``` : save and decode token
* ```setUserStatus(status)``` : send new user status to API
* ```removedata``` : clean state data (also in survey)
* ```LogOut``` : disconnect token
* ```LogOutFinish``` : disconnect token when survey is finished

  
## Mutations

Only not obvious mutations are documented here
