---
title: Index
summary: Store principle
authors:
- Antoine Masson
---

## General

* The store uses Vuex for sharing functions among the entire project.
* The store enables logging in console when not in production.

* [Admin Store](./admin/index.md) : Admin store is used in admin project.
* [Client Store](./client/index.md) : Client store is used in admin project.