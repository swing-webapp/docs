---
title: Template variables
summary: Global variables used in client side for translating the client interface
authors:
- Antoine Masson
---

## Principle

The client web application uses template variables that are language dependant (such as button text, labels ...).

They are stored in a mango table and managed from the admin interface.

Each variable has a unique name in uppercase, it can be string (without formatting) or html (with formatting, pictures ...).

## Add/Edit variable

!!! note
    Only modify template variables if you know what you are doing. You can break the entire website by doing something wrong.

* To manage language, go into ```Surveys > Language > Template Variables```

* You can edit or remove a variable by using the table or create one by using the form at the bottom of the page.

## Import/Export/Template

* The API contains also a template file for language called ```Template File```. This template is used when a new instance of the system is created, so that variables are not empty.
* It is possible to replace the API template file (to update it for example) by clicking on ```Save actual to template file``` button at the bottom of the ```Surveys Language Management``` page.
* It is possible to replace all the languages by the template file (to restore languages for example) by clicking on ```Replace with template file``` button at the bottom of the ```Surveys Language Management``` page.

* Download all languages in a JSON file by clicking on ```Download Actual```.
* Replace the languages by uploading a JSON file.
