---
title: Index
summary: Information about source code
authors:
- Antoine Masson
---

## Docker

If you would like to create your own instance of the software, see [Docker](docker.md).


## Files Structure

Files Structure describes the purpose of each file of each project

* For the [admin app](./files_structure/admin.md)
* For the [client app](./files_structure/client.md)
* For the [API](./files_structure/api.md)

## Database Models

Database Models describes the schema of each table in the mongo database

* For the [survey](./models/survey.md)
* For the [token](./models/token.md)
* For the [user admin](./models/users.md)


## Store

Vue.js can be combined with [vuex](https://vuex.vuejs.org/) which allows the access to variables and functions for the entire project.

Informations about the store for:

* [client interface](./store/client/index.md)
* [admin interface](./store/admin/index.md)

## API

API does action through POST / GET / UPDATE HTTP command.

To know more about these commands see [API](API.md).

## Templates Language Variables

The client web application uses template variables that are language dependant (such as button texts, labels ...).

They are stored in a mango table and managed from the admin interface.

To know more, see [template variables](template_variables.md).

## Documentation

To know how this documentation is generated, see [docs](docs.md).
