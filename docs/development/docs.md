---
title: Documentation (this website)
summary: How to generate project documentation
authors:
- Antoine Masson
---

## Principle

This documentation is generated using [Mkdocs](https://www.mkdocs.org/). It converts markdown files into a static html website.

## Commands

* to preview the documentation : ```mkdocs serve```
* to build the documentation : ```mkdocs build```
* to generate the ```requirements.txt``` file :  ```pip freeze > requirements.txt```

## Install the necessary packages to compile the documentation

1. create a directory and go inside it
1.  ```python -m venv env```
1.  ```pip install -r requirements.txt```
1. ```source ./env/bin/activate```

