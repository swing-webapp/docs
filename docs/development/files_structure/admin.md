---
title: Admin
summary: Files structure of Admin 
authors:
- Antoine Masson
---

## Language

The admin interface uses a [VueJS](https://vuejs.org/) combined with a [vuex store](https://vuex.vuejs.org/). The graphical framework uses [quasar](https://www.quasar.dev/).

The packages are described in the ```package.json``` file at the root of the project.

## Files

    ./LICENSE : MIT License
    ./README.md
    ./babel.config.js
    ./ckeditor5 custom build of ckeditor replace the installed package
        ./ckeditor-build/ckeditor.js
        ./ckeditor-build/ckeditor.js.map
    ./filename.txt : this file lists generated using tree -ifI node_modules > filename.txt
    ./package-lock.json : npm file for managing packages
    ./package.json : npm list of command and packages installed for this project
    ./public : external file needed for the project
        ./public/favicon.ico : icon of the application
        ./public/index.html : index page for webserver
        ./public/logoeawag.png : Eawag logo
    ./src : project files
        ./src/App.vue : main Vue application
        ./src/assets : add features
            ./src/assets/countries.js : list of countries
            ./src/assets/flags : flags (in .svg)
    ./src/components : Vue components
            ./src/components/AddTokenBatch.vue
            ./src/components/DispJSON.vue
            ./src/components/EditAlternative.vue
            ./src/components/EditPOC.vue
            ./src/components/EditPrePostQuest.vue
            ./src/components/EditSurvey.vue
            ./src/components/EditToken.vue
            ./src/components/EditTranslation.vue
            ./src/components/EditUser.vue
            ./src/components/ListPrePostSurvey.vue
            ./src/components/ListToken.vue
            ./src/components/form_inputs : prepost survey form inputs
                ./src/components/form_inputs/buttons_input.vue
                ./src/components/form_inputs/buttons_setup.vue
                ./src/components/form_inputs/checkboxes_input.vue
                ./src/components/form_inputs/checkboxes_setup.vue
                ./src/components/form_inputs/dragdrop_input.vue
                ./src/components/form_inputs/dragdrop_setup.vue
                ./src/components/form_inputs/numerical_input.vue
                ./src/components/form_inputs/numerical_setup.vue
                ./src/components/form_inputs/numslide_input.vue
                ./src/components/form_inputs/numslide_setup.vue
                ./src/components/form_inputs/stickslide_input.vue
                ./src/components/form_inputs/stickslide_setup.vue
                ./src/components/form_inputs/string_input.vue
                ./src/components/form_inputs/string_setup.vue
                ./src/components/form_inputs/text_input.vue
                ./src/components/form_inputs/text_setup.vue
            ./src/components/inputs : Generic inputs
                ./src/components/inputs/OpenFile.vue : open a file
                ./src/components/inputs/PopupEditText.vue
                ./src/components/inputs/ReOrderItems.vue
                ./src/components/inputs/SelectLang.vue
                ./src/components/inputs/SurveyListInput.vue
                ./src/components/inputs/UploadSurvey.vue : popup used for downloading survey
        ./src/helpers : common functions
            ./src/helpers/api.js : for API communication
            ./src/helpers/dnd.js : for drag and drop inputs
        ./src/layouts : page layouts
            ./src/layouts/EditSurvey.vue : layout when editing survey
            ./src/layouts/Main.vue : layout of main interface
        ./src/main.js : configuration file for Vue and starting javascript file
        ./src/quasar.js : configuration file quasar 
        ./src/router : Vue Router
            ./src/router/index.js : Router config file
            ./src/router/modules
                ./src/router/modules/survey.js : route for survey
                ./src/router/modules/token.js : route for token
                ./src/router/modules/user.js : route for users
        ./src/store : Vuex store files
            ./src/store/index.js : config and main func for store
            ./src/store/modules
                ./src/store/modules/auth.js : func for authentication
                ./src/store/modules/survey.js : func for surveys
                ./src/store/modules/token.js : func for token
        ./src/styles : CSS Style
            ./src/styles/quasar.sass
            ./src/styles/quasar.variables.sass
        ./src/views : Main Vue View
            ./src/views/About.vue
            ./src/views/AdminIndex.vue
            ./src/views/GodControl.vue
            ./src/views/Help.vue
            ./src/views/Login.vue
            ./src/views/Surveys
                ./src/views/Surveys/AddSurvey.vue
                ./src/views/Surveys/Edit
                    ./src/views/Surveys/Edit/AddAltRSES.vue : Add alternative
                    ./src/views/Surveys/Edit/AddPostSurvQuest.vue : Add post survey question
                    ./src/views/Surveys/Edit/AddPreSurvQuest.vue  : Add pre survey question
                    ./src/views/Surveys/Edit/AddTokenES.vue : In edit survey : Add token
                    ./src/views/Surveys/Edit/AlternativeRSES.vue  : Manage alternatives (table)
                    ./src/views/Surveys/Edit/BatchTokenES.vue : In edit survey : add token in batch
                    ./src/views/Surveys/Edit/EditAltRSES.vue : Edit Alternative
                    ./src/views/Surveys/Edit/EditPostSurvQuest.vue : Edit post survey question
                    ./src/views/Surveys/Edit/EditPreSurvQuest.vue : Edit pre survey question
                    ./src/views/Surveys/Edit/EditTokenES.vue : In edit survey : edit token
                    ./src/views/Surveys/Edit/MainEditSurvey.vue : Main Page when editing survey
                    ./src/views/Surveys/Edit/ObjectivesRSES.vue :  Manage objectives 
                    ./src/views/Surveys/Edit/PostSurvES.vue :  Manage post survey (table)
                    ./src/views/Surveys/Edit/PreSurvES.vue : Manage pre survey (table)
                    ./src/views/Surveys/Edit/PredicMatRSES.vue : Manage prediction matrix (table)
                    ./src/views/Surveys/Edit/RationalSurvES.vue :  Manage general option of rational
                    ./src/views/Surveys/Edit/ResultsES.vue :  Manage results
                    ./src/views/Surveys/Edit/TokensES.vue :  Manage tokens (table)
                    ./src/views/Surveys/Edit/WelcomePageES.vue :  Manage welcome page
                ./src/views/Surveys/GlobalOptSurvey.vue
                ./src/views/Surveys/LanguagesAddTranslation.vue
                ./src/views/Surveys/LanguagesEditTranslation.vue
                ./src/views/Surveys/LanguagesTemplateSurvey.vue
                ./src/views/Surveys/LanguagesTranslationSurvey.vue
                ./src/views/Surveys/ListSurvey.vue
            ./src/views/Tokens
                ./src/views/Tokens/AddToken.vue
                ./src/views/Tokens/BatchToken.vue
                ./src/views/Tokens/EditToken.vue
                ./src/views/Tokens/GlobalListToken.vue
            ./src/views/Users
                ./src/views/Users/AddAdminUser.vue
                ./src/views/Users/EditAdminUser.vue
                ./src/views/Users/ListAdminUser.vue
    ./vue.config.js : configuration file for Vue
    ./yarn.lock : yarn file for managing packages
    
    20 directories, 351 files



