---
title: Client
summary: Files structure of client
authors:
- Antoine Masson
---

## Language

The client interface uses a [VueJS](https://vuejs.org/) combined with a [vuex store](https://vuex.vuejs.org/). The graphical framework uses [quasar](https://www.quasar.dev/).

The packages are described in the ```package.json``` file at the root of the project.


## Files

    ./LICENSE : MIT License
    ./README.md
    ./babel.config.js
    ./filename.txt : this file lists generated using tree -ifI node_modules > filename.txt
    ./package-lock.json : npm file for managing packages
    ./package.json : npm list of command and packages installed for this project
    ./public : external file needed for the project
        ./public/favicon.ico : icon of the application
        ./public/index.html : index page for webserver
        ./public/logoeawag.png : Eawag logo
    ./src : project files
        ./src/App.vue : main Vue application
        ./src/assets : add features
            ./src/assets/countries.js : list of countries
            ./src/assets/flags : flags (in .svg)
        ./src/components : Vue components
            ./src/components/AlternativesDisp.vue : Display a list of alternatives
            ./src/components/AlternativesSort.vue : Sort a list of alternatives
            ./src/components/Conclusion_rational.vue : Display the conclusion for rational survey
            ./src/components/ImgItem.vue : Display a box with a picture, a caption and info and click actions
            ./src/components/MidValue.vue : Midvalue method
            ./src/components/ObjectivesSort.vue : Swing and Trade-off sorting for objectives
            ./src/components/PrePostSurvey.vue : Pre or Post survey display
            ./src/components/SelectLang.vue : Language selection
            ./src/components/form_input : Form input for post and pre survey
                ./src/components/form_input/buttons_input.vue :
                ./src/components/form_input/checkboxes_input.vue
                ./src/components/form_input/dragdrop_input.vue
                ./src/components/form_input/numerical_input.vue
                ./src/components/form_input/numslide_input.vue
                ./src/components/form_input/stickslide_input.vue
                ./src/components/form_input/string_input.vue
                ./src/components/form_input/text_input.vue
            ./src/components/poc.vue : Person of contact display
            ./src/components/popup_card.vue : Display a popup for alternative or objective presentation
            ./src/components/popup_help.vue : Display a popup for methods display
            ./src/components/Swing.vue : Swing Method
            ./src/components/TradeOff.vue : Trade-off Method
        ./src/helpers : common functions
            ./src/helpers/api.js : for API communication
            ./src/helpers/dnd.js : for drag and drop inputs
            ./src/helpers/generic.js : diverse function for sorting, normalizing objects
            ./src/helpers/tradeoff.js : trade-off numerical function
        ./src/layouts : page layouts
            ./src/layouts/Main.vue : layout used in all the project
        ./src/main.js : configuration file for Vue and starting javascript file
        ./src/quasar.js : configuration file quasar 
        ./src/router : Vue Router
            ./src/router/index.js : Router config file
        ./src/store : Vuex store files
            ./src/store/index.js : config and main func for store
            ./src/store/modules
                ./src/store/modules/auth.js : func for authentication
                ./src/store/modules/survey.js : func for survey
        ./src/styles : CSS Style
            ./src/styles/quasar.sass
            ./src/styles/quasar.variables.sass
        ./src/views : Main Vue View
            ./src/views/AccessUrl.vue : Used when using direct access url entry
            ./src/views/Conclusion_rational_print : used for generating final report (printable)
            ./src/views/Login.vue : Login (main) page
            ./src/views/NotFoundComponent.vue : 404 errors
            ./src/views/PostSurvey.vue : Post survey
            ./src/views/PreSurvey.vue : Pre survey
            ./src/views/RationalSurvey.vue : Rational survey
            ./src/views/RedirectUrl.vue : Used when user is redirected to another url after the survey
            ./src/views/SurveyIndex.vue : This component is the hub when users arrive (after login, access...) it reloads previous data (if any) and redirects the users where they should go
            ./src/views/TokenUrl.vue : Used if token is entered in URL instead of login page
            ./src/views/WelcomePage.vue : display survey welcome page
    ./vue.config.js : configuration file for Vue
    ./yarn.lock : yarn file for managing packages
    
    13 directories, 304 files



## Commands

* ```npm serve``` : start the testing server.
* ```npm build``` : compile the project into static html and js (into /public).
