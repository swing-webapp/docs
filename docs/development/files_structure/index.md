---
title: Files structure
summary: File organization for the different systems of the project
authors:
- Antoine Masson
---
Files structure describes the purpose of each file of each project,

* For the [admin app](admin.md)
*  For the [API](api.md)
* For the [client app](client.md)
