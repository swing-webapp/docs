---
title: API
summary: Files structure of API
authors:
- Antoine Masson
---

## Language

The API runs a [expressJS](https://expressjs.com/) API server. The API needs to communicate with a mongo database.

The packages are described in the ```package.json``` file at the root of the project.


## Files

    ./LICENSE : MIT License
    ./README.md
    ./config : directory with config files
        ./config/API.js : Concern the configuration of the API
        ./config/DB.js : Concern the configuration of the DB connection (mongo)
    ./controllers : Controllers are functions used in the API
        ./controllers/SurveyController.js : Functions used for surveys
        ./controllers/TokenController.js: Functions used for token
        ./controllers/UserController.js : Functions used for users
    ./filename.txt : this file lists generated using tree -ifI node_modules > filename.txt
    ./helpers : function used for adding functionalities 
        ./helpers/errors.js : not used
        ./helpers/jsonvalidtor.js : modules for the validation of survey
    ./index.js : main file, used to initiate the API server
    ./middleware : function used between functions
        ./middleware/auth.js : manages authentication headers
        ./middleware/errorHandler.js : not used
    ./models : mongo database model definition
        ./models/AdminUserModel.js : model for users
        ./models/SurveyLanguageModel.js : model for languages (template and translation)
        ./models/SurveyModel.js : models for surveys
        ./models/TokenModel.js : model for token
    ./package-lock.json : npm file for managing packages
    ./package.json : npm list of commands and packages installed for this project
    ./public : external file needed or generated (for ex uploaded)
        ./public/template
            ./public/template/LanguageDef.json : template variable file definition (loaded if corresponding database is empty)
            ./public/template/LanguageTrans.json translation variable file definition (loaded if corresponding database is empty)
        ./public/uploads : not used
    ./routes : contains the different possible routes for the API
        ./routes/AdminUserRoute.js : route for users
        ./routes/Route.js : generic route
        ./routes/SurveyRoute.js : route for survey
        ./routes/TokenRoute.js : route for token
    ./startup.sh : server starting script

    9 directories, 25 files

## Commands

To run the server, use the command ```npm start```.

API commands are described in [API](../API.md).
