---
title: Docker
summary: Docker documentation
authors:
- Antoine Masson
---

## Principle

Docker is used for running the web application (composed of the client, admin, and docs) and the API. The web application is compiled into statics html/javascript whereas the API runs into a node webserver.
Everything is controlled by a unique webserver (nginx) which redirects the service based on the URL.

The API uses a mango database to store all the data.

When deployed, three dockers work together :

* The nginx webserver with a ```80(http)``` port
* The API expressJS node server which communicates with the nginx webserver
* The mongo database which communicates with the API

```mermaid
flowchart TD;
A[NGINX Webserver] ----> |/*| B1[Client] 
A ----> |/admin| B2[Admin]
A ----> |/admin/docs| B3[Docs]  
A ----> |/api/*| C1[API]  
C1 <--> D[Mongo Database]
```

## Docker commands

[docker-compose doc](https://docs.docker.com/compose/gettingstarted/)
[install docker](https://docs.docker.com/engine/install/)

### Create a new docker instance

To create a new docker instance use (for production version) :

1. ```git clone https://gitlab.switch.ch/swing-webapp/docker.git```
1. ```cd docker```
1. ```(sudo) docker-compose up```


!!! note
    You need to create an admin account when you create a new instance by going into the admin interface adding ```/admin/``` to the main URL.

!!! note
    When creating a new instance, you need to edit the survey global option in order for the system to work properly. See [Global Options](../survey/global_options.md).

### Other commands

* To stop : ```(sudo) docker-compose stop```
* To stop and erase data : ```(sudo) docker-compose down```
* To run in the background ```(sudo) docker-compose up -d ```
* To rebuild (and replace) when running in the background ```(sudo) docker-compose up -d --build```

## Production instance

The gitlab address of development code is [https://gitlab.switch.ch/swing-webapp](https://gitlab.switch.ch/swing-webapp)

The URL docker git is ```https://gitlab.switch.ch/swing-webapp/docker.git```


### File structure

    ./docker-compose.yaml : the main docker file that uses docker-compose to combine all the containers together
    ./docker-sshkey.pub : the public key shared with the gitlab instance
    ./nginx-app.conf : the config file of the nginx webserver
    ./LICENSE : MIT license
    ./Dockerfile-api : The dockerfile to create the API docker
    ./README.md : Repository read me
    ./docker-sshkey : the private key shared with the gitlab instance
    ./Dockerfile-app : The dockerfile to create the webserver docker

## Development instance

!!! note
    You need access to this private gitlab repository.

The gitlab address of development code is [https://gitlab.youmi-lausanne.ch/eawag-aubert/swing-webapp](https://gitlab.youmi-lausanne.ch/eawag-aubert/swing-webapp).

The URL docker git is ```ssh://git@gitlab.youmi-lausanne.ch:50/eawag-aubert/swing-webapp/docker.git```

### File structure

    ./docker-compose.yaml : the main docker file that uses docker-compose to combine all the containers together
    ./docker-sshkey.pub : the public key shared with the gitlab instance
    ./nginx-app.conf : the config file of the nginx webserver
    ./LICENSE : MIT license
    ./Dockerfile-api : The dockerfile to create the API docker
    ./uploads : not used here
    ./README.md : Repository read me
    ./docker-sshkey : the private key shared with the gitlab instance
    ./Dockerfile-app : The dockerfile to create the webserver docker

