---
title: Index
summary: Add/Edit Global variables used in client side for translating the client interface
authors:
- Antoine Masson
---

## Principle

* To enable a language in surveys it needs to be created first.
* A language is defined by its name, a flag and a series of global variables used in the template of the client interface.
* To manage language go into ```Surveys > Language > Available Translation```.

## Add/Edit language

* A language is defined by its names, it will be in uppercase and must be unique.
* A flag is associated to the language.
* Flag and Name will be displayed in the client interface for choosing the language.

## Edit variable

* The template contains variables that need to be filled.
* Variables can be :
    * *text*, so no formatting is possible.
    * *html* (with a text editor) so they can be formatted and contain pictures, movies...

!!! note
    * It is possible to add or edit variables after creation.
    * Modifying a variable will modify the content of every survey using this language even if the survey was created before the modification.
    * All variables must be filled otherwise the client system will fail to work.

## Import/Export/Template

* The API contains also a template file for language called ```Template File``` this template is used when a new instance of the system is created, so the languages are not empty.
* It is possible to replace the API template file (to update it for example) by clicking on ```Save actual to template file``` button at the bottom of the Surveys Language Management page.
* It is possible to replace all the language by the template file (to restore languages for example) by clicking on ```Replace with template file``` button at the bottom of the Surveys Language Management page.
* Download all languages in a ZIP by clicking on ```Download Actual```.
* Replace the languages by uploading a ZIP.

!!! note
    * The ZIP has a specific format and it should be respected.
    * At the root of the ZIP you should have the json file named ```LangTrans.json```.
    * Images should be located in folders with the following structure : ``<LANG>/<VARIABLENAME>/file``.
    * Non-used images will be automatically removed during upload.
    * The name of the ZIP does not matter.
    * All the languages are stored in one file, uploading a file erase all the languages already present in the system.
